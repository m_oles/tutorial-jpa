package db;

public enum DatabaseType {
    POSTGRES,
    MYSQL
}
