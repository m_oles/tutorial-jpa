package db;

import entities.Cash;
import entities.Info;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class DatabaseOperation {

    private final DatabaseType databaseType;
    private final EntityManagerFactory entityManagerFactory;

    public DatabaseOperation(DatabaseType databaseType) {
        this.databaseType = databaseType;
        switch (databaseType) {
            case MYSQL:
                entityManagerFactory = Persistence.createEntityManagerFactory("mysql");
                break;
            case POSTGRES:
                entityManagerFactory = Persistence.createEntityManagerFactory("postgres");
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

    public List<Cash> getCash(String cash){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Cash> criteriaBuilderQuery = criteriaBuilder.createQuery(Cash.class);
        Root<Cash> root = criteriaBuilderQuery.from(Cash.class);
        criteriaBuilderQuery.where(criteriaBuilder.equal(root.get("cash"), cash));
        Query query = entityManager.createQuery(criteriaBuilderQuery);
        List<Cash> result = new ArrayList<Cash>(query.getResultList());
        return result;
    }

    public List<Cash> getCash(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("FROM Cash");
        List<Cash> result = new ArrayList<Cash>(query.getResultList());
        return result;
    }

    public List<Info> getAllInfo(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("FROM Info");
        List<Info> result = new ArrayList<Info>(query.getResultList());
        return result;
    }

    public void saveOrUpadateInfo(Info info){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(info);
        entityManager.getTransaction().commit();
    }



}
