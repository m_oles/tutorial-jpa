package entities;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;


@Entity
@Table(name = "CASH")
public class Cash {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "DAY")
    private LocalDateTime day;

    @Column(name = "CASH")
    private String cash;

    @Column(name = "VALUE")
    private Double value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDay() {
        return day;
    }

    public void setDay(LocalDateTime day) {
        this.day = day;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cash cash1 = (Cash) o;
        return Objects.equals(id, cash1.id) &&
                Objects.equals(day, cash1.day) &&
                Objects.equals(cash, cash1.cash) &&
                Objects.equals(value, cash1.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, day, cash, value);
    }

    @Override
    public String toString() {
        return "Cash{" +
                "id=" + id +
                ", day=" + day +
                ", cash='" + cash + '\'' +
                ", value=" + value +
                '}';
    }
}
