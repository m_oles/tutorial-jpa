import db.DatabaseOperation;
import db.DatabaseType;
import entities.Cash;
import entities.Info;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by moles on 03.10.2016.
 */
public class Runner {
    public static void main(String[] args) {
        DatabaseOperation databaseOperation = new DatabaseOperation(DatabaseType.POSTGRES);
        //---------------------select
        List<Cash> cash;
        System.out.println("All Cash");
        cash=databaseOperation.getCash();
        readCash(cash);
        System.out.println("-------------------------------");
        System.out.println("Cash By 'JPY'");
        cash = databaseOperation.getCash("JPY");
        readCash(cash);
        System.out.println("-------------------------------");
        //--------------------Insert
        int size = databaseOperation.getAllInfo().size();
        System.out.println("-------------------------------");
        System.out.println("Before insert, number: "+size);
        Info info=new Info();
        info.setInfo(LocalDateTime.now().toString());
        databaseOperation.saveOrUpadateInfo(info);
        List<Info> allInfo = databaseOperation.getAllInfo();
        System.out.println("After insert, number: "+allInfo.size());
        readInfo(allInfo);
        return;
    }

    public static void readCash(List<Cash> cash){
        cash.forEach(el -> {
            System.out.println(el.toString());
        });
    }

    public static void readInfo(List<Info> infoList){
       for(Info info: infoList ){
           System.out.println(info.toString());
       }
    }
}
